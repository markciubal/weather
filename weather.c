#include <stdio.h>
#include <libsocket/libinetsocket.h>
#include <string.h>

const int SIZE = 50;

struct weatherData
{
    char observation[SIZE];
    char location[SIZE];
    char temperature[SIZE];
    char humidity[SIZE];
    char wind_dir[SIZE];
    char wind_mph[SIZE];
};

struct forecastData
{
    char weekday[SIZE];
    char high[SIZE];
    char low[SIZE];
};

int main(int argc, char *argv[])
{
    struct weatherData currentWeather;
    int fd = create_inet_stream_socket("api.wunderground.com", "80", LIBSOCKET_IPv4, 0);
    FILE *f = fdopen(fd, "r+");
    if (!f)
    {
        
    }
    fprintf(f, "GET /api/1655f919bbcd29ed/conditions/q/%s.json HTTP/1.0\n", argv[1]);
    fprintf(f, "Host: api.wunderground.com\n");
    fprintf(f, "\n");
    char line[1000];
    int location_flag = 0;
    while (fgets(line, 1000, f) != NULL)
    {
        char observation_time_final[SIZE];
        // Observation Time
        char *observation_time;
        const char otime[SIZE] = "observation_time\":\"";
        observation_time = strstr(line, otime);
        if (observation_time)
        {
            if(sscanf(observation_time, "%*[^\"]\":\"%50[^\"]", observation_time_final) == 1)
            {
                strcpy(currentWeather.observation, observation_time_final);
            }
        }
        
        char location_final[SIZE];
        // Observation Time
        char *location;
        const char loc[SIZE] = "full\":\"";
        location = strstr(line, loc);
        if (location)
        {
            if (location_flag == 0)
            {
                if(sscanf(location, "%*[^\"]\":\"%50[^\"]", location_final) == 1)
                {
                    strcpy(currentWeather.location, location_final);
                    location_flag = 1;
                }
            }
        }

        char temperature_final[SIZE];
        // Observation Time
        char *temperature;
        const char temp[SIZE] = "temperature_string\":\"";
        temperature = strstr(line, temp);
        if (temperature)
        {
            if(sscanf(temperature, "%*[^\"]\":\"%50[^,]", temperature_final) == 1)
            {
                sscanf(temperature_final, "%s", temperature_final);
                //printf("%s", temperature_final);
                //temperature_final[3] = '\0';
                //printf("t_final: %s", temperature_final);
                for (int i = 0; i < strlen(temperature_final); i++)
                {
                    if (temperature_final[i] == '\"')
                    {
                        temperature_final[i] = '\0';
                    }
                }
                
                strcpy(currentWeather.temperature, temperature_final);
            }
        }
        
        char humidity_final[SIZE];
        // Observation Time
        char *humidity;
        const char hum[SIZE] = "relative_humidity\":\"";
        humidity = strstr(line, hum);
        if (humidity)
        {
            if(sscanf(humidity, "%*[^\"]\":\"%50[^\"]", humidity_final) == 1)
            {
                strcpy(currentWeather.humidity, humidity_final);
            }
        }
        
        char wind_dir_final[SIZE];
        // Observation Time
        char *wind_dir;
        const char wd[SIZE] = "wind_dir\":\"";
        wind_dir = strstr(line, wd);
        if (wind_dir)
        {
            if(sscanf(wind_dir, "%*[^\"]\":\"%50[^\"]", wind_dir_final) == 1)
            {
                strcpy(currentWeather.wind_dir, wind_dir_final);
            }
        }
        
        char wind_mph_final[SIZE];
        // Observation Time
        char *wind_mph;
        const char wm[SIZE] = "wind_mph\":";
        wind_mph = strstr(line, wm);
        if (wind_mph)
        {
            if(sscanf(wind_mph, "%*[^\"]\":%50[^,]", wind_mph_final) == 1)
            {
                strcpy(currentWeather.wind_mph, wind_mph_final);
            }
        }
        
        //return(0);
        //printf("%s", line);
    }
    fclose(f);
    destroy_inet_socket(fd);
    
    printf("Current Conditions\n");
    printf("Observation time: %s\n", currentWeather.observation);
    printf("Location: %s\n", currentWeather.location);
    printf("Temperature: %s F\n", currentWeather.temperature);
    printf("Humidity: %s\n", currentWeather.humidity);
    printf("Wind: %s %s mph\n", currentWeather.wind_dir, currentWeather.wind_mph);
    
    int fd4 = create_inet_stream_socket("api.wunderground.com", "80", LIBSOCKET_IPv4, 0);
    FILE *f4 = fdopen(fd4, "r+");
    if (!f4)
    {
        
    }
    fprintf(f4, "GET http://api.wunderground.com/api/1655f919bbcd29ed/conditions/forecast10day/q/%s.json HTTP/1.0\n", argv[1]);
    fprintf(f4, "Host: api.wunderground.com\n");
    fprintf(f4, "\n");
    struct forecastData day1_data;
    struct forecastData day2_data;
    struct forecastData day3_data;
    struct forecastData day4_data;
    char line4[1000];
    int forecast_flag = 0;
    int weekday_flag = 0;
    int line_count = 0;
    while (fgets(line4, 1000, f4) != NULL)
    {
        
        // For finding weekday keys in the JSON data.
        char weekday[SIZE];
        char *wkd;
        const char wd[SIZE] = "weekday\":\"";
        wkd = strstr(line4, wd);
        char weekday_final[SIZE];
        
        // For finding temperature keys in the JSON data.
        char temperature[SIZE];
        char *temp;
        const char t[SIZE] = "fahrenheit\":\"";
        temp = strstr(line4, t);
        char temperature_final[SIZE];

        // Start weekday grabbing.
        if (wkd && weekday_flag == 0)
        {
            if (sscanf(wkd, "%*[^\"]\":\"%50[^\"]", weekday_final) == 1)
            {
                strcpy(day1_data.weekday, weekday_final);
                weekday_flag++;
            }
        } else if (wkd && weekday_flag == 1)
        {
            if (sscanf(wkd, "%*[^\"]\":\"%50[^\"]", weekday_final) == 1)
            {
                strcpy(day2_data.weekday, weekday_final);
                weekday_flag++;
            }
        } else if (wkd && weekday_flag == 2)
        {
            if (sscanf(wkd, "%*[^\"]\":\"%50[^\"]", weekday_final) == 1)
            {
                strcpy(day3_data.weekday, weekday_final);
                weekday_flag++;
            }
        } else if (wkd && weekday_flag == 3)
        {
            if (sscanf(wkd, "%*[^\"]\":\"%50[^\"]", weekday_final) == 1)
            {
                strcpy(day4_data.weekday, weekday_final);
                weekday_flag++;
            }
        }
        
        // Start temperature grabbing.
        if (temp && forecast_flag == 0)
        {
            if (sscanf(temp, "%*[^\"]\":\"%50[^\"]", temperature_final) == 1)
            {
                strcpy(day1_data.high, temperature_final);
                forecast_flag++;
            }
        } else if (temp && forecast_flag == 1)
        {
            if (sscanf(temp, "%*[^\"]\":\"%50[^\"]", temperature_final) == 1)
            {
                strcpy(day1_data.low, temperature_final);
                forecast_flag++;
            }
        } else if (temp && forecast_flag == 2)
        {
            if (sscanf(temp, "%*[^\"]\":\"%50[^\"]", temperature_final) == 1)
            {
                strcpy(day2_data.high, temperature_final);
                forecast_flag++;
            }
        } else if (temp && forecast_flag == 3)
        {
            if (sscanf(temp, "%*[^\"]\":\"%50[^\"]", temperature_final) == 1)
            {
                strcpy(day2_data.low, temperature_final);
                forecast_flag++;
            }
        } else if (temp && forecast_flag == 4)
        {
            if (sscanf(temp, "%*[^\"]\":\"%50[^\"]", temperature_final) == 1)
            {
                strcpy(day3_data.high, temperature_final);
                forecast_flag++;
            }
        } else if (temp && forecast_flag == 5)
        {
            if (sscanf(temp, "%*[^\"]\":\"%50[^\"]", temperature_final) == 1)
            {
                strcpy(day3_data.low, temperature_final);
                forecast_flag++;
            }
        } else if (temp && forecast_flag == 6)
        {
            if (sscanf(temp, "%*[^\"]\":\"%50[^\"]", temperature_final) == 1)
            {
                strcpy(day4_data.high, temperature_final);
                forecast_flag++;
            }
        } else if (temp && forecast_flag == 7)
        {
            if (sscanf(temp, "%*[^\"]\":\"%50[^\"]", temperature_final) == 1)
            {
                strcpy(day4_data.low, temperature_final);
                forecast_flag++;
            }
        }
        if (line_count < 500)
        {
            //printf("%s", line4);
            line_count++;
        }
    }
    printf("\nForecast\n");
    printf("%s: %s %s\n", day1_data.weekday, day1_data.high, day1_data.low);
    printf("%s: %s %s\n", day2_data.weekday, day2_data.high, day2_data.low);
    printf("%s: %s %s\n", day3_data.weekday, day3_data.high, day3_data.low);
    printf("%s: %s %s\n", day4_data.weekday, day4_data.high, day4_data.low);
}